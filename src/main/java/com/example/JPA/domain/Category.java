package com.example.JPA.domain;


public enum Category {
		
	PROGRAMMING("Programming"), 
	FICTION("Fiction"), 
	ROMANCE("Romance"), 
	DOCUMENTARY("Documentary"),
	COMP_PROGRAMMING("Computer Programming");

	
	public String category;
	
	private Category(String category) {
		this.setCategory(category);
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public static Category of(String category) {
		for(Category r : Category.values()) {
            if (r.category.equalsIgnoreCase(category))
                return r;
        }
        return null;
    }
    
	@Override
	public String toString() {
		return this.category;
	}
 


}
