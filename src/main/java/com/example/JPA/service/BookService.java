package com.example.JPA.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.JPA.entities.Book;
import com.example.JPA.repositories.BookRepo;

@Service
public class BookService {
	
	
	private BookRepo bookRepo;
	
	@Autowired
	public BookService(BookRepo bookRepo) {
		this.bookRepo = bookRepo;
	}
	
	public Iterable<Book> lookup(){
		return bookRepo.findAll();
		
	}
	
	public Book addBook(String author, String category, String title, int pages, float price,
			String publication) {
		Book book = bookRepo.findByTitle(title);
		if(book != null) {
			throw new RuntimeException("Book Already exist");
		}
		
		
		return bookRepo.save(book);
		
		
		
	}
	

}
