package com.example.JPA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@ComponentScan({"com.example.JPA"})
//@EntityScan("com.delivery.domain")
//@EnableJpaRepositories("com.example.JPA.repo")
public class JpaHateoasApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaHateoasApplication.class, args);
	}

}
