package com.example.JPA.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.example.JPA.domain.Category;

@Entity
@Table(name="book")
public class Book implements Serializable{
	
	private static final long serialVersionUID = 2910360270359969219L;

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String author;
	
	@Transient
	private Category category; //ignore this field using @transient
	
	@Column(name="category") //re-assigned the column into new variable
	private String dbCategory;
	
	@Column
	private String title;
	
	@Column
	private int pages;
	
	@Column
	private float price;
	
	@Column
	private String publication;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getPublication() {
		return publication;
	}
	public void setPublication(String publication) {
		this.publication = publication;
	}
	
    public String getDbCategory() {
		return dbCategory;
	}
	public void setDbCategory(String dbCategory) {
		this.dbCategory = dbCategory;
	}
	
	/**
	 *  Actual solution.
	 *  The idea behind this is to have two fields one for application use and one for database. 
	 *  Synchronization of the fields is performed using @PostLoad and @PrePersist callbacks.
	 *  Before saving an entity we are setting a value from the application field (ENUM) to database one, 
	 *  and after loading an entity we are updating the application value based on the database one (saved value)
	 * 
	 */
	@PrePersist
    public void prePersistent() {
        if (this.category != null) {
            this.dbCategory = category.toString();
        }
    }
	    
    @PostLoad
    public void postLoad() {
        if (this.dbCategory != null) {
            this.category = Category.of(this.dbCategory);
        }
    }
}
