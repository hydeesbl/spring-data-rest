package com.example.JPA.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.JPA.entities.Book;

//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete

//@RepositoryRestResource(collectionResourceRel = "booksale", path="booksale")
public interface BookRepo extends CrudRepository<Book, Integer>{

	Book findByTitle( @Param("title") String title);

}
